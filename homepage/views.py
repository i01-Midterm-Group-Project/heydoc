from django.contrib.auth import authenticate, login, get_user_model, logout
from django.views.generic import CreateView, FormView
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render,redirect
from django.utils.http import is_safe_url
# import requests

from homepage.forms import LoginForm, RegisterForm

class LoginView(FormView):
    form_class = LoginForm
    success_url = '/homepage/'
    template_name = 'login.html'

    def form_valid(self, form):
        request = self.request
        next_ = request.GET.get('next')
        next_post = request.POST.get('next')
        redirect_path = next_ or next_post or None
        email  = form.cleaned_data.get("email")
        password  = form.cleaned_data.get("password")
        user = authenticate(request, username=email, password=password)
        if user is not None:
            login(request, user)
            try:
                del request.session['guest_email_id']
            except:
                pass
            if is_safe_url(redirect_path, request.get_host()):
                return redirect(redirect_path)
            else:
                return redirect("/homepage/")
        return super(LoginView, self).form_invalid(form)

class RegisterView(CreateView):
    form_class = RegisterForm
    template_name = 'registration.html'
    success_url ='/homepage/'

def homepage(request):
    return render(request, 'homepage.html')

def logout_request(request):
	logout(request)
	return redirect("/homepage/")

# def homepage(request):
#     data = requests.get("https://apicovid19indonesia-v2.vercel.app/api/indonesia")
#     data_json = data.json()
#     context = {'data':data_json[0]}
#     return render(request, "homepage.html", context)

# def covid_api(request):
#     data = requests.get("https://apicovid19indonesia-v2.vercel.app/api/indonesia")
#     data_json = data.json()
#     return JsonResponse(data_json, safe = False)
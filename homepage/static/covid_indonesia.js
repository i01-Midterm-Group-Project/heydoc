setInterval(() => {
    $.ajax({
        type: "GET",
        url: "https://apicovid19indonesia-v2.vercel.app/api/indonesia",
        data: {},

        success: function(result) {
            $("#positif").text(result.positif);
            $("#dirawat").text(result.dirawat);
            $("#meninggal").text(result.meninggal);
            $("#sembuh").text(result.sembuh);
        },
    });
    }, 1);
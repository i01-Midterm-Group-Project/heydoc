from django.db import models

# Create your models here.
class Drugs(models.Model):
    Drugs_name = models.CharField(max_length = 50)
    Drugs_description = models.TextField()
    Drugs_image = models.ImageField(upload_to = 'DrugsImg/')
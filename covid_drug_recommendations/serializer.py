from rest_framework.serializers import ModelSerializer
from .models import Drugs

class DrugsSerializer(ModelSerializer):
    class Meta:
        model = Drugs
        fields = '__all__'

        
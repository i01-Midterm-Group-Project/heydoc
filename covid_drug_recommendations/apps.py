from django.apps import AppConfig


class CovidDrugRecommendationsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'covid_drug_recommendations'

from django.urls import path
from covid_drug_recommendations.views import index
from covid_drug_recommendations.views import getDrugs
from covid_drug_recommendations.views import postDrugs


urlpatterns = [
    path('', index, name='index'),
    # create a path for the API
    path('getDrugs/', getDrugs, name='getDrugs'),
    path ('postDrugs/', postDrugs, name='postDrugs')
    ]
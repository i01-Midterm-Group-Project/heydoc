from django.shortcuts import render
from covid_drug_recommendations.models import Drugs
from django.template import loader
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializer import DrugsSerializer

# Create your views here.
def index(request):
    response = {'data': Drugs.objects.all()}
    return render(request, 'coviddrug.html',response)

@api_view(['GET'])
def getDrugs(request):
    drugs = Drugs.objects.all()
    serializer = DrugsSerializer(drugs, many=True)
    return Response(serializer.data)

@api_view(['POST'])
def postDrugs(request):
    serializer = DrugsSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=201)
    return Response(serializer.errors, status=400)
    
from django.urls import path
from django.urls.conf import include
from . import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register("DaftarVacc", views.DaftarVaccApi)

urlpatterns = [
    path('', views.index, name="VaccinationInfo"),
    path('get/daftar/', views.GetDaftarVacc),
    path('get/place/', views.GetVaccPlace),
    path('create/', include(router.urls)),
    path('createForm/', views.CreateDaftarVacc)
]

from rest_framework.fields import CharField, SerializerMethodField
from rest_framework.serializers import ModelSerializer, Serializer, SerializerMetaclass
import rest_framework.serializers as serializers
from .models import *

class DaftarVaccSerializer(ModelSerializer):
    class Meta:
        model = DaftarVacc
        fields = '__all__'

class VaccPlaceSerializer(ModelSerializer):
    class Meta:
        model = VaccPlace
        fields = '__all__'
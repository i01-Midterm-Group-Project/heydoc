from django.apps import AppConfig


class VaccinationInfoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'vaccination_info'

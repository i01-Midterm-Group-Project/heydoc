from django.contrib import admin

from vaccination_info.models import VaccPlace, DaftarVacc

admin.site.register(VaccPlace)
admin.site.register(DaftarVacc)

# Register your models here.

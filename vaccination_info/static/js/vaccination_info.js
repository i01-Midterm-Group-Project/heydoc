function initMap(){
  map = new google.maps.Map(document.getElementById('map'), {
      mapId: '477f14090cb54997',
      center: {lat: -6.207403223324002, lng: 106.83075067821207},
      zoom: 11,
      backgroundColor: '#00ADB5',
    });

const markers = [
["In Harmony Clinic", -6.185008518851763, 106.86286170405786, "Jl. Percetakan Negara IV B No.48, RT.9/RW.9, Johar Baru, Kec. Johar Baru, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10560"],
["Rumah Vaksinasi Pusat", -6.282788849931928, 106.86088759677455, "Jalan Raya Inpres No. 81 RT05/RW09, Kel. Tengah, Kramat Jati, RT.2/RW.9, Kampung Tengah, Kramatjati, RT.2/RW.9, Kp. Tengah, Kec. Kramat jati, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13540"],
["Tempat Vaksin Jakarta Pusat", -6.142682612642584, 106.867067405954, "Jl. Serdang III No.1, RW.5, Serdang, Kec. Kemayoran, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10650"],
["KKP Halim Perdana Kusumah", -6.253268962826751, 106.87668044245545, "Jl. Jengki No.8, RT.8/RW.2, Kb. Pala, Kec. Makasar, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13650"],
["Dokter Vaksin", -6.159749916903419, 106.86157424223887, "Jl. Serdang Raya No.16, RT.8/RW.4, Serdang, Kec. Kemayoran, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10640"],
["Sentra Vaksin JIEP",-6.196773517075468, 106.91998296767233,"9, Jatinegara, Cakung, East Jakarta City, Jakarta"],
["Klinik Vaksinasi Vaxcorp Indonesia", -6.144549480939189, 106.88977056778242, "Komp. Plaza Kelapa Gading. Ruko Inkopal Jl. Raya Boulevard Barat Block C/51, RT.2/RW.9, Klp. Gading Bar., Kec. Klp. Gading, Kota Jkt Utara, Daerah Khusus Ibukota Jakarta 14240"],
["Balai Besar Pelatihan Kesehatan", -6.230173684097686, 106.79135799260122, "Jl. Hang Jebat No.F3, RW.8, Gunung, Kec. Kby. Baru, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12120"],
["Klinik KASIH ProSehat", -6.198773600332513, 106.78449153767475, "Ruko ProSehat, Jl. Palmerah Barat No.45A, RT.2/RW.3, Grogol Utara, Kec. Kby. Lama, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12210"]
];

for(let i = 0; i<markers.length; i++){
const currMarker = markers[i]
const marker = new google.maps.Marker({
  position:{lat: currMarker[1], lng: currMarker[2]},
  map: map,
  title: currMarker[0],
  animation: google.maps.Animation.DROP
});

const infowindow = new google.maps.InfoWindow({
  content: '<h2 id="firstHeading" class="firstHeading">' + currMarker[0].bold() + '</h2>'  + '<h6>' + currMarker[3] + '</h6>' +  '<br>' + '<button onclick="Scroll()" type="button" id="btnScroll">Register</button>'
});

var currentInfoWindow = null;

marker.addListener("click", () => {
  if (currentInfoWindow != null) {
    currentInfoWindow.close();
    }
  infowindow.open(map, marker);
  currentInfoWindow = infowindow;
});
}
}


//-6.207403223324002, 106.83075067821207
from django import forms
from django.db.models.fields.related import ForeignKey
from .models import VaccPlace, DaftarVacc


class DateInput(forms.DateInput):
    input_type = 'date'
class DaftarVaccineForm(forms.ModelForm):
    VaccDate = forms.DateField(widget=DateInput)   

    class Meta:
        model = DaftarVacc
        fields = ('name', 'NIK', 'VaccPlace', 'VaccDate')

        widget = {
        'name': forms.TextInput(attrs={'class': 'form-control'}),
        'NIK': forms.NullBooleanSelect(attrs={'class': 'form-control'}),
        'VaccPlace': forms.TextInput(attrs={'class': 'form-select'}),
        'VaccDate': forms.DateInput(attrs={'class': 'form-control'}),
        }
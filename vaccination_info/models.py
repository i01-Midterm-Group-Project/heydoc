from django.db import models
from datetime import date

class VaccPlace(models.Model):
    name = models.CharField(max_length=50)
    def __str__(self):
        return self.name
    


class DaftarVacc(models.Model):
    name = models.CharField(max_length=30)
    NIK = models.CharField(max_length=16)
    VaccPlace = models.ForeignKey(VaccPlace, on_delete=models.SET_NULL, blank= True, null= True)
    VaccDate = models.DateField(default= date.today)
    def __str__(self):
        return self.name

import re
from django.db.models import query
from django.shortcuts import render
from rest_framework.serializers import Serializer
from .models import VaccPlace, DaftarVacc
from .forms import DaftarVaccineForm
from django.http.response import HttpResponseRedirect, JsonResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import *
from rest_framework import viewsets

# Create your views here.

def index (request):
    form = DaftarVaccineForm()
    # if request.method == 'POST':
    #     form = DaftarVaccineForm(request.POST)
    #     if form.is_valid():
    #         form.save()
    #         return HttpResponseRedirect("/vaccination_info")
    if request.is_ajax():
        if request.method == 'POST':
            form = DaftarVaccineForm(request.POST)
            if form.is_valid():
                form.save()
                return JsonResponse({
                    'msg':'success'
                }) 
    return render(request, 'vaccination_info.html', {'form': form})

@api_view(['GET'])
def GetDaftarVacc(request):
    daftar = DaftarVacc.objects.all()
    serializer = DaftarVaccSerializer(daftar, many = True)
    return Response(serializer.data)

@api_view(['GET'])
def GetVaccPlace(request):
    place = VaccPlace.objects.all()
    serializer = VaccPlaceSerializer(place, many = True)
    return Response(serializer.data)

@api_view(['POST'])
def CreateDaftarVacc(request):
    data = request.data
    place = VaccPlace.objects.get(name = str(data['VaccPlace']))
    daftar = DaftarVacc.objects.create(
        name = data['name'], 
        NIK = data['NIK'],
        VaccPlace = place,
        VaccDate = data['VaccDate']
    )
    serializer = DaftarVaccSerializer(daftar, many = False)
    return Response(serializer.data)

class DaftarVaccApi(viewsets.ModelViewSet):
    queryset = DaftarVacc.objects.all()
    serializer_class = DaftarVaccSerializer
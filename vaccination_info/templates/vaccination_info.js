function initMap(){
    map = new google.maps.Map(document.getElementById('map'), {
        mapId: '477f14090cb54997',
        center: {lat: -6.207403223324002, lng: 106.83075067821207},
        zoom: 11,
      });

const markers = [
  ["In Harmony Clinic", -6.185008518851763, 106.86286170405786],
  ["Rumah Vaksinasi Pusat", -6.282788849931928, 106.86088759677455],
  ["Tempat Vaksin Jakarta Pusat", -6.142682612642584, 106.867067405954],
  ["KKP Halim Perdana Kusumah", -6.253268962826751, 106.87668044245545],
  ["Dokter Vaksin", -6.159749916903419, 106.86157424223887]
];

for(let i = 0; i<markers.length; i++){
  const currMarker = markers[i]
  const marker = new google.maps.Marker({
    position:{lat: currMarker[1], lng: currMarker[2]},
    map: map,
    title: currMarker[0],
    icon:{
      url: "coronavirus-vaccine-svgrepo-com.svg",
      scaledSize: new google.maps.Size(38, 31)
    },
    animation: google.maps.Animation.DROP
  });
  
  const infowindow = new google.maps.InfoWindow({
    content: currMarker[0]
  });
  
  marker.addListener("click", () => {
    infowindow.open(map, marker);
  });
  }
}

//-6.207403223324002, 106.83075067821207
from django import forms
from .models import UserInput

class UserForm(forms.ModelForm):
    class Meta:
        model = UserInput
        fields = "__all__"

const postForm = document.querySelector("#ajaxform");

function handleSubmit(postForm) {
    postForm.addEventListener("submit", e => {
        e.preventDefault();
        formData = new FormData(postForm);
        fetch('/suggestions/', {
                method: 'POST',
                body: formData,
            })
            .then(response => response.text())
            .then(data => {
                postForm.reset();
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    })
}

handleSubmit(postForm)
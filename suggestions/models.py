from django.db import models

class UserInput(models.Model):
    name = models.CharField(max_length=24)
    text = models.TextField(max_length=128)
    star = models.IntegerField(default=0)

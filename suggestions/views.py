from django.shortcuts import render
from .models import UserInput
from django.http import HttpResponseRedirect, HttpResponse
from .forms import UserForm
from django.db.models import Avg
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def index(request):
    if request.method == "GET":
        index = UserInput.objects.all()
        rating_json = index.aggregate(Avg("star"))
        total_submissions = index.count()
        rating_json["star__avg"] = "{:.2f}".format(rating_json["star__avg"])
        response = {'notes': index, 'rating' : rating_json, 'total_submissions' : total_submissions}

    if request.method == "POST":
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/suggestions")
    return render(request, "index_suggestions.html", response)


$(function () {
    $(".doctor_card_appointment").click(function () {
        $(".doctor_card_appointment").removeClass("clicked");
        $(this).toggleClass("clicked");
        $doctor_id = $(this).attr('id')
        

        $.ajax({
            url: "http://localhost:8000/schedule-an-appointment/get-hospital",
            type: "GET",
            data: { doctor_id: $doctor_id },
            success: function (data) {
                
                var returnedData = JSON.parse(data);
                var display = '<div class="ajax_container">'
                for (let index = 0; index < returnedData.length; index++) {
                    const element = returnedData[index];
                    display += "<div class='hospital_card' id='"+ element.fields.hospital +"'>"
                    display += "<p>"+ element.fields.hospital +"</p>";
                    display += "<button type='button' class='time_button'>"+ element.fields.available_time1 +"</button>";
                    display += "<button type='button' class='time_button'>"+ element.fields.available_time2 +"</button>";
                    display += "<button type='button' class='time_button'>"+ element.fields.available_time3 +"</button></div>";

                }
                
                $(".ajax_hospital").html(display)


                $(".time_button").click(function () {
                    $("#input_hospital").val($(this).parent().attr('id'));
                    $("#input_doctor").val($doctor_id);
                    $("#input_time").val($(this).text());
                    $("#input_date").val("2021-09-04");
                })
            }

        });
    })
    
    

});


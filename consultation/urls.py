from django.urls import path
from .views import appointment_result, get_hospital, index, chat, appointment

urlpatterns = [
    path('', index, name='index'),
    path('chat-a-doctor/', chat, name='chat'),
    path('schedule-an-appointment/', appointment, name='appointment'),
    path('schedule-an-appointment/result/', appointment_result, name='appointment_result'), 
    path('schedule-an-appointment/get-hospital', get_hospital, name='get_hospital'),
]

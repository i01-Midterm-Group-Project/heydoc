from django.http import response, HttpResponse, JsonResponse
from django.core import serializers
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import Doctor, Schedule, HospitalSchedule
from .forms import ScheduleForm


# Create your views here.
def index(request):
    doctors = Doctor.objects.all()
    response = {'doctors' : doctors}
    return render(request, 'index.html', response)

def chat(request):
    return render(request, 'chat.html')

@login_required(login_url='/admin/login/')
def appointment(request):
    doctors = Doctor.objects.all()
    if request.method == 'POST':

        form = ScheduleForm(request.POST)

        if form.is_valid():
            patient = form.save(commit=False)
            patient.user = request.user
            form.save()
            return HttpResponseRedirect('https://heyheydoc.herokuapp.com/consultation/schedule-an-appointment/result')

    else:
        form = ScheduleForm()

    
    response = {'doctors' : doctors, 'form' : form}
    return render(request, 'appointment.html', response)

def get_hospital(request):
    doctor_id = request.GET.get('doctor_id')
    hospital_schedule = HospitalSchedule.objects.filter(doctor= doctor_id)
    data = serializers.serialize('json', hospital_schedule)
    return JsonResponse(data, safe=False)


def appointment_result(request):
    schedules = Schedule.objects.filter(user=request.user)
    response = {'schedules' : schedules}
    return render(request, 'appointment_result.html', response)
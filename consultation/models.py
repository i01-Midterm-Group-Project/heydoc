from django.db import models
from django.conf import settings
from django.contrib.auth.models import User


# Create your models here.


# Doctor model/user
class Doctor(models.Model):
    name = models.CharField(max_length=50)
    specialization = models.CharField(default="Dokter Umum", max_length=50)
    image = models.ImageField(default=None)
    status = models.BooleanField(default=False)
    rating = models.IntegerField(default=0)

# Hospital model, include available timeslots per doctor
class HospitalSchedule(models.Model):
    doctor = models.ForeignKey('Doctor', on_delete=models.CASCADE)
    hospital = models.CharField(default=None, max_length=50)
    available_time1 = models.TimeField(default=None)
    available_time2 = models.TimeField(default=None, blank=True, null=True)
    available_time3 = models.TimeField(default=None, blank=True, null=True)

# Chat model, rooms and messages

# Schedule model, include user, doctor, place, and time
class Schedule(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete= models.CASCADE)
    doctor = models.ForeignKey('Doctor',default=None, on_delete=models.CASCADE)
    hospital = models.CharField(default=None, max_length=50)
    date = models.DateField(auto_now=False, auto_now_add=False)
    time = models.TimeField(default=None)
    
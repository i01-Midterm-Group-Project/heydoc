# Generated by Django 3.2.7 on 2021-10-29 02:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('consultation', '0002_auto_20211029_0943'),
    ]

    operations = [
        migrations.AlterField(
            model_name='doctor',
            name='status',
            field=models.BooleanField(default=False),
        ),
    ]

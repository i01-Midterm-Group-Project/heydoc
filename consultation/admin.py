from django.contrib import admin

from consultation.models import Doctor, HospitalSchedule, Schedule

# Register your models here.

admin.site.register(Doctor)
admin.site.register(HospitalSchedule)
admin.site.register(Schedule)
from django.urls import path
from .views import HomeView, getItem, product_detail, item_list, add_to_cart, remove_from_cart,add, OrderSummaryView, remove_single_item_from_cart, search_item, categorised_item, getItem, getOrderItem, getOrder
urlpatterns = [
    path('', HomeView.as_view(), name = 'index'),
    path('product', OrderSummaryView.as_view(), name = 'product'),
    path('item', item_list, name = 'item_list'),
    path('add-to-cart/<slug>/', add_to_cart, name  ='add-to-cart'),
    path('remove-from-cart/<slug>/', remove_from_cart, name = "remove-from-cart"),
    path('add',add,name = "add"),
    path('remove-item-from-cart/<slug>/', remove_single_item_from_cart,
         name='remove-single-item-from-cart'),
    path('search',search_item, name = 'search'),
    path('category/<str:value>/', categorised_item, name = 'category'),
    path('category/all/add', add, name = "add"),
    path('category/C/add', add, name = "add"),
    path('category/A/add', add, name = "add"),
    path('category/H/add', add, name = "add"),
    path('category/V/add', add, name = "add"),
    path('category/F/add', add, name = "add"),
    path('category/S/add', add, name = "add"),
    path('category/I/add', add, name = "add"),
    path('category/D/add', add, name = "add"),
    path('category/O/add', add, name = "add"),
    path('datas/item/', getItem),
    path('datas/orderitem/', getOrderItem),
    path('datas/order/', getOrder),



]
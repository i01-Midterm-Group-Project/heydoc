from django.shortcuts import get_object_or_404, redirect, render
from django.views.generic import ListView, DetailView, View
from .models import Item, OrderItem, Order
from django.utils import timezone
from django.contrib import messages
from django.http import HttpResponse, JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
import json
import time
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializer import *



@api_view(['GET'])
def getItem(request):
    items = Item.objects.all()
    serializer = ItemSerializer(items, many = True)
    return Response(serializer.data)

@api_view(['GET'])
def getOrderItem(request):
    items = OrderItem.objects.all()
    serializer = OrderItemSerializer(items, many = True)
    return Response(serializer.data)

@api_view(['GET'])
def getOrder(request):
    items = Order.objects.all()
    serializer = OrderSerializer(items, many = True)
    return Response(serializer.data)




class HomeView(LoginRequiredMixin, ListView):
    login_url = '/homepage/login'
    model = Item
    template_name = "buy.html"




class OrderSummaryView(LoginRequiredMixin, View):
    login_url = '/homepage/login'
    def get(self, *args, **kwargs):
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            context = {
                'object': order
            }
            return render(self.request, 'product.html', context)
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("/buy/category/all/")



def product_detail(request):
    response = {}
    return render(request, 'product.html',response)


def item_list(request):
    context = {
        'items': Item.objects.all()
    }
    return render(request, 'item_list.html', context)


def add_to_cart(request, slug):
    item = get_object_or_404(Item, slug = slug)
    order_item,created = OrderItem.objects.get_or_create(
        item=item,
        user = request.user,
        ordered = False
        )
    order_qs = Order.objects.filter(user=request.user, ordered = False)
    if order_qs.exists():
        order = order_qs[0]
        if order.items.filter(item__slug = item.slug).exists():
            order_item.quantity += 1    
            order_item.save()
        else:
            order.items.add(order_item)
    else:
        ordered_date = timezone.now()
        order = Order.objects.create(user=request.user, ordered_date=ordered_date)
        order.items.add(order_item)
    return redirect("product")


@csrf_exempt  
def add(request):
    request.session.set_expiry(0)
    if request.is_ajax():

        request.session['orders'] = request.POST.get('orders')
        order = json.loads(request.session['orders'])
        for ids in order:
            item = get_object_or_404(Item, slug = ids[0])
            order_item,created = OrderItem.objects.get_or_create(
                item=item,
                user = request.user,
                ordered = False
                )
            order_qs = Order.objects.filter(user=request.user, ordered = False)
            if order_qs.exists():
                order = order_qs[0]
                if order.items.filter(item__slug = item.slug).exists():
                    order_item.quantity += 1    
                    order_item.save()
                else:
                    order.items.add(order_item)
            else:
                ordered_date = timezone.now()
                order = Order.objects.create(user=request.user, ordered_date=ordered_date)
                order.items.add(order_item)
    return HttpResponse('')







def remove_from_cart(request, slug):
    item = get_object_or_404(Item, slug=slug)
    order_qs = Order.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.items.filter(item__slug=item.slug).exists():
            order_item = OrderItem.objects.filter(
                item=item,
                user=request.user,
                ordered=False
            )[0]
            order.items.remove(order_item)
            order_item.delete()
            return redirect("product")
        else:
            return redirect("index", slug=slug)
    else:
        return redirect("index", slug=slug)


def remove_single_item_from_cart(request, slug):
    item = get_object_or_404(Item, slug=slug)
    order_qs = Order.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.items.filter(item__slug=item.slug).exists():
            order_item = OrderItem.objects.filter(
                item=item,
                user=request.user,
                ordered=False
            )[0]
            if order_item.quantity > 1:
                order_item.quantity -= 1
                order_item.save()
            else:
                order.items.remove(order_item)
                order_item.delete()
            return redirect("product")
        else:
            return redirect("index", slug=slug)
    else:
        return redirect("index", slug=slug)


def search_item(request):
    if request.method == "POST":
        searched = request.POST['searched']
        if searched != "":
            items = Item.objects.filter(title__contains=searched)
        else:
            return redirect('/buy/')
        return render(request, 'buy.html', {'searched' : searched, 'items':items})
    else:
        pass

@login_required(login_url= '/homepage/login')
def categorised_item(request,value):
    if value != 'all':
        items = Item.objects.filter(category = value)
    if value == 'V':
        value_string = 'Vitamin'
    elif value == 'C':
        value_string = 'Covid-19'
    elif value == 'A':
        value_string = 'Alergi'
    elif value == 'F':
        value_string = 'Flu'
    elif value == 'S':
        value_string = 'Suplemen Makanan'
    elif value == 'I':
        value_string = 'Kebutuhan Ibu'
    elif value == 'D':
        value_string = 'Diabetes'
    elif value == 'O':
        value_string = 'Persendian dan Otot'
    elif value == 'H':
        value_string = 'Herbal'
    else:
        value_string = 'All Product'
        items = Item.objects.all()
    
    p = Paginator(items, 12)
    page_n = request.GET.get('page', 1)

    try:
        page = p.page(page_n)
    except EmptyPage:
        page = p.pages(1)



    return render(request, 'buy.html', {'object_list':page, 'values': value_string, 'values_category': value, 'page_list': page})

@csrf_exempt
def show_data(request):
    items = Item.objects.all()
    data = {}
    for item in items:
        list_item = [{'title': item.title, 'price': item.price, 'slug': item.slug}]
        data.update({item.title: list_item[0]})
    time.sleep(10)
    return JsonResponse(data, safe=False)
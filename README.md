# HEYDOC
### Hello and welcome to our project called HEYDOC.
### 🖥Group Members🖥
1. 2006519883 - Bilhuda Pramana
2. 2006489842 - Muhammad Athaqil Makarim
3. 2006519845 - Ainul Malik Zaidan Ismail
4. 2006489060 - Danish Rafid Rajendra
5. 2006489874 - Firlandi Althaf Rizqi Ansyari
6. 2006488013 - Fauzan Nazranda Rizqan
7. 2006519920 - Ezekiel Nicholas Harjadri 

### 🔗OUR WEBSITE LINK🔗
[OUR WEBSITE](https://heyheydoc.herokuapp.com/)

### ⚙Description⚙
An innovation from us to decrease the number of Covid-19 in Indonesia, we assist by offering Education and Information in the form of Vaccines, Statistics, and Recommendations for Covid Medicines, as well as consultations with Covid specialist doctors and the supply of Covid drugs.

### 🕹HEYDOC Modules🕹
-  Homepage(Registration and login,Admin panel) (Aqil)
-  Vaccination info (Where can you obtain vaccinations, what vaccines are accessible, the goal is vaccination information.) (Danish)
- Statistics on Vaccines and Covids (Together)
- Covid drug recommendations and Covid preventive How to (Billy)
- Consultation (Malik)
- place to buy medicine (Fauzan)
- Covid-Related Articles and News (Ezekiel)
- Suggestions and criticism. (Landi)

### 🥼Buyers Persona🥼
<img src="/uploads/55c69f385316f8f394c6ea05f410577b/1.png" alt="alt text" width=380>
<img src="/uploads/b258ff0b6e5da1299323edcf91132fdb/2.png" alt="alt text" width=380><br>
<img src="/uploads/7372159e5ace6d4390fde43f636e35bb/3.png" alt="alt text" width=380>
<img src="/uploads/154773a31eca02cd83aee6b2b667602e/Persona_4.png" alt="alt text" width=380>


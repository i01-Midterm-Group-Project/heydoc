from rest_framework.serializers import ModelSerializer
from .models import *

class ItemSerializer(ModelSerializer):
    class Meta:
        model = Article
        fields = '__all__'
from django.urls import path
from . import views
import article
app_name = 'article'
urlpatterns = [
    path('', views.index, name='artikel'), 
    path('getArticles', views.getArticles, name='getArticles'),
    path('create', views.create, name='create'),
    path('news', views.article, name= 'article'),
    path('datas', views.getItem, name= 'datas'),
    path('addArticle', views.addItem, name= 'addArticle'), 
]
import json
from typing import List
from django.http.response import HttpResponse, HttpResponseRedirectBase
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .forms import ArticleForm
from .models import Article
from django.http import JsonResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializer import *


# Create your views here.
def index(request): 
    return render(request, 'form_article.html')

def getArticles(request):
    articles = Article.objects.all()
    return JsonResponse({"articles":list(articles.values())})

def create(request):
    if request.method == 'POST':
        title = request.POST['title']    
        article = request.POST['article']   
        source = request.POST['source']  
        

        new_articles = Article(title=title, article=article, source=source)
        new_articles.save()
        return HttpResponse('New Articles Created Successfully')

def article(request):
    article = Article.objects.all()
    return render(request, 'article.html', {'article':article})     


@api_view(['GET'])
def getItem(request):
    items = Article.objects.all()
    serializer = ItemSerializer(items, many = True)
    return Response(serializer.data)

@csrf_exempt
def addItem(request):
    if request.method == 'POST':
        data = json.loads(request.body)

        title_flutter = data['title']
        article_flutter = data['article']
        source_flutter = data['source']
        

        article_form = Article(title = title_flutter, article = article_flutter, source = source_flutter)

        
        article_form.save()
        return JsonResponse ({"status": "success"}, status = 200)

    
    else:    
        return JsonResponse ({"status": "error"}, status = 401)
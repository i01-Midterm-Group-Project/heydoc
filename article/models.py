from django.db import models
from django.utils.timezone import now

class Article(models.Model):
    title = models.TextField()
    article = models.TextField()
    source = models.TextField()
    datetime = models.DateTimeField(default=now)
    